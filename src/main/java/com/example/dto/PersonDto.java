package com.example.dto;

import lombok.Data;

@Data
public class PersonDto {
    private String name;
    private String profession;
}
